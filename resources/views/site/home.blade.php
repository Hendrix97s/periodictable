<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tabela Periódica</title>
    <link rel="stylesheet" href="css/styleHome.css">
    <link rel="stylesheet" href="css/all.css">
    
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
</head>
<body>
    <nav>
        <div id="navbar" class="navbar">
            <div class="logo">
            <!-- <i class="fas fa-dragon"></i> -->
            </div>

            <div id="menu" class="menu">
                <ul>
                    <li>Inicio</li>
                    <li>Qual o intuito</li>
                    <li>Jogo</li>
                    <li>Ajude-nos</li>
                    <li>Entrar</li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
            <div id="containerElements" class="containerElements">
                <div class="containerDescript">
                    <div class="imgElement" id="img"></div>

                    <div class="description">
                        <div class="containerDataElements">
                            <!-- <div class="labelName"><strong>Nome:  </strong></div> -->
                            <div id="nameElement" class="itemContainerElement itemNameElement">---</div>
                            

                            <div class="itemContainerElement labelSymbol"><strong>Símbolo:</strong></div>
                            <div id="symbolElement" class="itemContainerElement symbolElement">---</div>
                            
                            
                            <div class="itemContainerElement labelNumberAtomic"><strong>Número Atômico:</strong></div>
                            <div id="atomicNumberElement" class="itemContainerElement atomicNumberElement">---</div>
                            
                            
                            <div class="itemContainerElement labelAtomicMass"><strong>Peso Atômico:</strong></div>
                            <div id="avarageAtomicMassElement" class="itemContainerElement avarageAtomicMassElement">---</div>

                        </div>
                    </div>
                </div>       
            </div>

            <div class="containerDescriptionOfElements">

                <div class="imgElementRight" id="imgRight"></div>
                
                <div id="tableDescriptionElement" class="tableDescriptionElement">
                    <div class="item-left">Densidade</div>
                    <div id="densiy" class="item-right">...</div>
                    
                    <div class="item-left">Ponto de fusão</div>
                    <div id="meltingPointData" class="item-right">...</div>
                    
                    <div class="item-left">Ponto de ebulição</div>
                    <div id="boilingPoinData" class="item-right">...</div>
                </div>
                
                <div id="descriptionElement" class="descriptionElement">...</div>
            </div>

    </div><!-- fim container -->
    
    <script src="js/all.js"></script>
    <script src="js/actionHome.js"></script>
    
</body>
</html>