<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TablePeriodicController extends Controller
{
    //
    public function index(){
        $table = file_get_contents('js/periodicTable.json');
        return response()->json($table,201);
        // return $table;
    }
}
