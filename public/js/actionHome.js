

let containerElements
$.ajax({
    url: '/periodic',
    global: false,
    type: 'GET',
    // data: {},
    async: false, //blocks window close
    success: function (e) {
        containerElements = JSON.parse(e);


    }
});

function tablePeriodic() {
    let groupElements = document.getElementById('containerElements');
    let groupLatanideos = document.createElement('div')
    let groupActinideos = document.createElement('div')

    groupLatanideos.setAttribute('id', 'lantanideos')
    groupLatanideos.setAttribute('class', 'lantanideos')

    groupActinideos.setAttribute('id', 'actinideos')
    groupActinideos.setAttribute('class', 'actinidios')

    groupElements.appendChild(groupLatanideos)
    groupElements.appendChild(groupActinideos)

    let lantanideos = document.getElementById('lantanideos')
    let actinideos = document.getElementById('actinideos')

    let indexGroup = 1;
    let indexPeriod = 1;


    while (indexGroup <= 18) {
        let idNgroup = `group${indexGroup}`
        let nGroup = document.createElement('div');

        nGroup.setAttribute('id', `${idNgroup}`)
        nGroup.setAttribute('class', `group`)

        groupElements.appendChild(nGroup)



        while (indexPeriod <= 9) {
            let group = document.getElementById(`${idNgroup}`)
            let id = `element-${indexGroup}-${indexPeriod}`
            let el = document.createElement('div');
            el.setAttribute('id', `${id}`);
            el.setAttribute('class', 'el');
            el.setAttribute('onmouseover', 'showDataElement(this.id)');

            if (indexPeriod == 8) {
                lantanideos.appendChild(el)
            }
            else if (indexPeriod == 9) {
                actinideos.appendChild(el)
            }
            else {
                group.appendChild(el)
            }
            let getElements = containerElements.elements
            let positionElement = document.getElementById(`${id}`)

            indexEl = 0

            while (indexEl < getElements.length) {
                if (getElements[indexEl].group == indexGroup && getElements[indexEl].period == indexPeriod) {

                    let number = document.createElement('div')
                    let symbol = document.createElement('div')
                    let mass = document.createElement('div')

                    number.setAttribute('id', 'numberAtomic')
                    number.setAttribute('class', 'numberAtomic')

                    symbol.setAttribute('id', 'symbol')
                    symbol.setAttribute('class', 'symbol')

                    mass.setAttribute('id', 'mass')
                    mass.setAttribute('class', 'mass')

                    positionElement.appendChild(number)
                    positionElement.appendChild(symbol)
                    positionElement.appendChild(mass)

                    number.innerHTML = `${getElements[indexEl].atomicNumber}`
                    symbol.innerHTML = `${getElements[indexEl].symbol}`
                    mass.innerHTML = `${getElements[indexEl].avarageAtomicMass}`

                    classElement(positionElement, getElements[indexEl].elementClass)

                }
                indexEl++;
            }

            indexPeriod++;
        }

        indexGroup++;
        indexPeriod = 1;
    }

}




function showDataElement(id) {
    let element = id.split('-')
    let nameElement = document.getElementById('nameElement')
    let symbolElement = document.getElementById('symbolElement')
    let atomicNumberElement = document.getElementById('atomicNumberElement')
    let avarageAtomicMassElement = document.getElementById('avarageAtomicMassElement')
    let descriptionElement = document.getElementById('descriptionElement')
    let elementUsage = document.getElementById('elementUsage')
    let img = document.getElementById('img')
    let imgRight = document.getElementById('imgRight')


    // console.log(`${element[1]}-${element[2]}`);
    let index = 0;
    while (index < containerElements.elements.length) {
        if (containerElements.elements[index].group == element[1] && containerElements.elements[index].period == element[2]) {
            // console.log(containerElements.elements[index]);
            let itemElement = containerElements.elements[index]
            let nodImg = `<img src="/img/elements/${itemElement.img}">`

            nameElement.innerHTML = itemElement.name
            img.innerHTML = nodImg
            imgRight.innerHTML = nodImg
            symbolElement.innerHTML = itemElement.symbol
            atomicNumberElement.innerHTML = itemElement.atomicNumber
            avarageAtomicMassElement.innerHTML = itemElement.avarageAtomicMass
            descriptionElement.innerHTML = `<i>"${itemElement.description}"</i>`
            // elementUsage.innerHTML = itemElement.elementUsage
        }
        index++
    }

    // group: "17"
    // period: "6"
    // symbol: "At"
    // name: "shrewd"
    // atomicNumber: "85"
    // avarageAtomicMass: "210"
    // elementClass: "halogens"

    // if (containerElements.elements[].group 
}


function classElement(positionElement, typeElement) {

    switch (typeElement) {
        case 'nonmetals': //não metais
            positionElement.style.backgroundColor = "#a9dc57"
            positionElement.style.color = "#0000009c"
            break
        case 'transition metals': //metais de transição
            positionElement.style.backgroundColor = "#eeb2b0"
            positionElement.style.color = "#0000009c"
            break
        case 'alkaline earth metals': //metais alcalinos terrestres

            positionElement.style.backgroundColor = "#ece366"
            positionElement.style.color = "#0000009c"
            break
        case 'alkali metals': //metais alcalinos

            positionElement.style.backgroundColor = "#f3ca5c"
            positionElement.style.color = "#0000009c"
            break
        case 'lanthanides'://lantanídeos

            positionElement.style.backgroundColor = "#7fb6b8"
            positionElement.style.color = "#0000009c"
            break
        case 'actinides'://actinídeos

            positionElement.style.backgroundColor = "#a07c9a"
            positionElement.style.color = "#0000009c"
            break
        case 'post-transition metals'://metais pós-transição

            positionElement.style.backgroundColor = "#a0c6d1"
            positionElement.style.color = "#0000009c"
            break
        case 'metalloids'://metalóides
            positionElement.style.backgroundColor = "#76caba"
            positionElement.style.color = "#0000009c"
            break
        case 'halogens'://halogênios

            positionElement.style.backgroundColor = "#b5e2f1"
            positionElement.style.color = "#0000009c"
            break
        case 'noble gases'://gases nobres
            positionElement.style.backgroundColor = "#87b2d6"
            positionElement.style.color = "#0000009c"
            break
        case 'unknow'://desconhecido
            positionElement.style.backgroundColor = "#ccc"
            positionElement.style.color = "#0000009c"
            break;

    }

}


tablePeriodic()
